from trainerbase.gameobject import GameInt, GameFloat, GameByte
from trainerbase.memory import pm, Address


money = GameInt(pm.base_address + 0x77CE50)

hp = GameFloat(Address(pm.base_address + 0x76F3B8, [0x540]))
max_hp = GameFloat(Address(pm.base_address + 0x76F3B8, [0x544]))
inf_stamina_flag = GameByte(pm.base_address + 0x77CEE4)
friend_hp = GameFloat(Address(pm.base_address + 0x80992C, [0x540]))

weapon_slot_size = 0x1C
pistol = GameInt(Address(pm.base_address + 0x76F3B8, [0x5AC], weapon_slot_size * 2))
shotgun = GameInt(Address(pm.base_address + 0x76F3B8, [0x5AC], weapon_slot_size * 3))
submachine = GameInt(Address(pm.base_address + 0x76F3B8, [0x5AC], weapon_slot_size * 4))
assault = GameInt(Address(pm.base_address + 0x76F3B8, [0x5AC], weapon_slot_size * 5))
sniper = GameInt(Address(pm.base_address + 0x76F3B8, [0x5AC], weapon_slot_size * 6))
heavy = GameInt(Address(pm.base_address + 0x76F3B8, [0x5AC], weapon_slot_size * 7))
explosive = GameInt(Address(pm.base_address + 0x76F3B8, [0x5AC], weapon_slot_size * 8))
tool = GameInt(Address(pm.base_address + 0x76F3B8, [0x5AC], weapon_slot_size * 9))

lowrider_mission_score = GameInt(pm.base_address + 0x64A948)
dancing_mission_score = GameInt(pm.base_address + 0x64EC20)

graffiti = GameInt(pm.base_address + 0x69AD74)
horseshoes = GameInt(pm.base_address + 0x7791E4)
oysters = GameInt(pm.base_address + 0x7791EC)

denise = GameInt(pm.base_address + 0x649EFC)
michelle = GameInt(pm.base_address + 0x649F00)
helen = GameInt(pm.base_address + 0x649F04)
barbara = GameInt(pm.base_address + 0x649F08)
katie = GameInt(pm.base_address + 0x649F0C)
millie = GameInt(pm.base_address + 0x649F10)

gravity = GameFloat(pm.base_address + 0x463984)

player_x = GameFloat(Address(pm.base_address + 0x76F3B8, [0x14, 0x30]))
player_y = GameFloat(Address(pm.base_address + 0x76F3B8, [0x14, 0x34]))
player_z = GameFloat(Address(pm.base_address + 0x76F3B8, [0x14, 0x38]))
